package com.phiennv.productservcie.configuration;

import com.phiennv.productservcie.filter.JWTTokenValidatorFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
public class SecurityConfig {

    @Autowired
    JWTTokenValidatorFilter jwtFilter;

    @Bean
    SecurityFilterChain defaultSecurityFilterChain(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .addFilterBefore(jwtFilter, BasicAuthenticationFilter.class)
                .authorizeHttpRequests(
                        requests -> requests
                                .requestMatchers(new AntPathRequestMatcher("/api/product/create")).hasRole("ADMIN")
                                .requestMatchers(new AntPathRequestMatcher("/api/product/update")).hasRole("ADMIN")
                                .requestMatchers(new AntPathRequestMatcher("/api/product/delete")).hasRole("ADMIN")
                                .requestMatchers(new AntPathRequestMatcher("/upload/image")).hasRole("ADMIN")
                                .anyRequest().permitAll()
                );
        return http.build();
    }
}
