package com.phiennv.productservcie.constant;

public interface ConstantGlobal {
    public class ERROR_CODE {
        public static final String BLANK_CODE = "E001";
        public static final String USERNAME_ALREADY_EXIST_CODE = "E002";
        public static final String EMAIL_ALREADY_EXIST_CODE = "E002";
        public static final String NUMBER_PHONE_ALREADY_EXIST_CODE = "E003";
        public static final String UNAUTHORIZED_CODE = "E004";
        public static final String USER_NOT_FOUND_CODE = "E005";
        public static final String JWT_CODE = "E006";
    }

    public class MESSAGE_CODE {
        public static final String SUCCESS = "SUCCESS";
    }

    public class ROLE {
        public static final String USER_ROLE = "USER_ROLE";

        public static final String ADMIN_ROLE = "ADMIN_ROLE";
    }
}
