package com.phiennv.productservcie.specification;

import com.phiennv.productservcie.model.Product;
import org.springframework.data.jpa.domain.Specification;

public class ProductSpecification {
    private ProductSpecification() {
        throw new IllegalStateException("Utility class");
    }
    public static Specification<Product> likeName(String name) {
        return (root, query, cb) -> cb.like(root.get("name"), "%" + name + "%");
    }

    public static Specification<Product> hasPrice(float price) {
        return (root, query, cb) -> cb.equal(root.get("price"), price);
    }

    public static Specification<Product> lessPrice(int price) {
        return (root, query, cb) -> cb.lessThan(root.get("price"), price);
    }

    public static Specification<Product> greaterPrice(int price) {
        return (root, query, cb) -> cb.greaterThan(root.get("price"), price);
    }

    public static Specification<Product> hasQuality(int quantity) {
        return (root, query, cb) -> cb.equal(root.get("quantity"), quantity);
    }

    public static Specification<Product> lessQuality(int quantity) {
        return (root, query, cb) -> cb.lessThan(root.get("quantity"), quantity);
    }

    public static Specification<Product> greaterQuality(int quantity) {
        return (root, query, cb) -> cb.greaterThan(root.get("quantity"), quantity);
    }
}
