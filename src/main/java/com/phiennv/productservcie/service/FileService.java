package com.phiennv.productservcie.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileService {
    String uploadFile(String path, MultipartFile multipartFile) throws IOException;
}
