package com.phiennv.productservcie.service.impl;

import com.phiennv.productservcie.dto.ProductRequestForm;
import com.phiennv.productservcie.model.Product;
import com.phiennv.productservcie.repository.ProductRepository;
import com.phiennv.productservcie.service.ProductService;
import com.phiennv.productservcie.specification.ProductSpecification;
import com.phiennv.productservcie.utils.SpecificationUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Override
    public Product getProduct(Integer id) {
        return productRepository.findById(id).get();
    }

    @Override
    public void createProduct(ProductRequestForm productRequestForm) {
        Product product = new Product();
        product.setName(productRequestForm.getName());
        product.setAvatarPath(productRequestForm.getImagesUrl());
        product.setDescription(productRequestForm.getDescribe());
        product.setListOfColor(productRequestForm.getListOfColor());
        product.setPrice(productRequestForm.getRating());
        product.setQuantity(productRequestForm.getQuantity());
        product.setPrice(productRequestForm.getPrice());
        productRepository.save(product);
    }

    @Override
    public List<Product> search(ProductRequestForm productRequestForm) {
        List<Specification<Product>> conditions = new ArrayList<>();
        if (StringUtils.isNotEmpty(productRequestForm.getName())) {
            conditions.add(ProductSpecification.likeName(productRequestForm.getName()));
        }
        if (productRequestForm.getPrice() > 0) {
            conditions.add(ProductSpecification.hasPrice(productRequestForm.getPrice()));
        }

        Integer quality = productRequestForm.getQuantity();
        if (quality != null && quality > 0) {
            conditions.add(ProductSpecification.hasQuality(productRequestForm.getQuantity()));
        }

        Specification<Product> spec = SpecificationUtil.buildANDSpecification(conditions);
        PageRequest page = PageRequest.of(0,10);
        return productRepository.findAll(spec, page).get().collect(Collectors.toList());
    }

    @Override
    public void updateProduct(ProductRequestForm productRequestForm) {
        Product product = new Product();
        product.setName(productRequestForm.getName());
        product.setAvatarPath(productRequestForm.getImagesUrl());
        product.setDescription(productRequestForm.getDescribe());
        product.setListOfColor(productRequestForm.getListOfColor());
        product.setPrice(productRequestForm.getRating());
        product.setQuantity(productRequestForm.getQuantity());
        product.setPrice(productRequestForm.getPrice());
        productRepository.save(product);
    }

    @Override
    public void deleteProducts(List<Integer> ids) {
        productRepository.deleteAllById(ids);
    }
}
