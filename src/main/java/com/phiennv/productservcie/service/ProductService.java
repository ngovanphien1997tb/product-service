package com.phiennv.productservcie.service;


import com.phiennv.productservcie.dto.ProductRequestForm;
import com.phiennv.productservcie.model.Product;

import java.util.List;

public interface ProductService {

   Product getProduct(Integer id);

   List<Product> search(ProductRequestForm productRequestForm);
   void createProduct(ProductRequestForm productRequestForm);

   void updateProduct(ProductRequestForm productRequestForm);

   void deleteProducts(List<Integer> ids);
}
