package com.phiennv.productservcie.utils;

import com.phiennv.productservcie.model.Product;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public class SpecificationUtil {
    private SpecificationUtil() {

    }

    public static Specification<Product> buildANDSpecification(List<Specification<Product>> specifications){
        Specification<Product> conditions = null;
        if (!specifications.isEmpty()) {
            conditions = Specification.where(specifications.get(0));
            if(specifications.size() > 1) {
                for (int index = 1; index < specifications.size(); index = index + 1) {
                    conditions = conditions.and(specifications.get(index));
                }
            }
        }

        return conditions;
    }
}
