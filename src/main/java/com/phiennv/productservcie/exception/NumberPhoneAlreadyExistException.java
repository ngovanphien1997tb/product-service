package com.phiennv.productservcie.exception;

public class NumberPhoneAlreadyExistException extends RuntimeException{
    public NumberPhoneAlreadyExistException(String message) {
        super(message);
    }
}
