package com.phiennv.productservcie.exception;

public class AuthenticateException extends RuntimeException{
    public AuthenticateException(String message) {
        super(message);
    }
}
