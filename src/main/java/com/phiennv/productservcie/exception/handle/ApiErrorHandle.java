package com.phiennv.productservcie.exception.handle;

import com.phiennv.productservcie.constant.ConstantGlobal;
import com.phiennv.productservcie.dto.ApiMessage;
import com.phiennv.productservcie.dto.ApiResponse;
import com.phiennv.productservcie.exception.AuthenticateException;
import com.phiennv.productservcie.exception.EmailAlreadyExistsException;
import com.phiennv.productservcie.exception.NumberPhoneAlreadyExistException;
import com.phiennv.productservcie.exception.UsernameAlreadyExistsException;
import com.phiennv.productservcie.utils.MessageUtils;
import io.jsonwebtoken.SignatureException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestControllerAdvice
public class ApiErrorHandle {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ApiResponse> handleBlankException(MethodArgumentNotValidException e, WebRequest request) {
        final String lang = getLanguage(request);
        List<ApiMessage> messages = new ArrayList<>();
        e.getBindingResult().getFieldErrors().forEach(
                (error) -> {
                    String errorMessage = MessageUtils.getMessage(error.getDefaultMessage(), lang);
                    messages.add(ApiMessage.builder().code(error.getField())
                                    .content(errorMessage)
                            .build());
                }
        );
        return ResponseEntity.badRequest()
                .body(ApiResponse.createResponse(HttpStatus.BAD_REQUEST, messages, null));
    }

    @ExceptionHandler(UsernameAlreadyExistsException.class)
    public ResponseEntity<ApiResponse> handleUsernameAlreadyException(UsernameAlreadyExistsException e, WebRequest request) {
        final String lang = getLanguage(request);
        String errorMessage = MessageUtils.getMessage(e.getMessage(), lang);
        ApiMessage apiMessage = ApiMessage.builder().code(ConstantGlobal.ERROR_CODE.USERNAME_ALREADY_EXIST_CODE).content(errorMessage).build();
        return ResponseEntity.badRequest().body(ApiResponse.createResponse(HttpStatus.BAD_REQUEST
                , Collections.singletonList(apiMessage), null));
    }

    @ExceptionHandler(EmailAlreadyExistsException.class)
    public ResponseEntity<ApiResponse> handleEmailAlreadyException(EmailAlreadyExistsException e, WebRequest request) {
        final String lang = getLanguage(request);
        String errorMessage = MessageUtils.getMessage(e.getMessage(), lang);
        ApiMessage apiMessage = ApiMessage.builder().code(ConstantGlobal.ERROR_CODE.EMAIL_ALREADY_EXIST_CODE).content(errorMessage).build();
        return ResponseEntity.badRequest().body(ApiResponse.createResponse(HttpStatus.BAD_REQUEST
                , Collections.singletonList(apiMessage), null));
    }

    @ExceptionHandler(NumberPhoneAlreadyExistException.class)
    public ResponseEntity<ApiResponse> handleNumberPhoneAlreadyException(NumberPhoneAlreadyExistException e, WebRequest request) {
        final String lang = getLanguage(request);
        String errorMessage = MessageUtils.getMessage(e.getMessage(), lang);
        ApiMessage apiMessage = ApiMessage.builder().code(ConstantGlobal.ERROR_CODE.NUMBER_PHONE_ALREADY_EXIST_CODE).content(errorMessage).build();
        return ResponseEntity.badRequest().body(ApiResponse.createResponse(HttpStatus.BAD_REQUEST
                , Collections.singletonList(apiMessage), null));
    }

    @ExceptionHandler(AuthenticateException.class)
    public ResponseEntity<ApiResponse> handleAuthenticateException(AuthenticateException e, WebRequest request) {
        final String lang = getLanguage(request);
        String errorMessage = MessageUtils.getMessage(e.getMessage(), lang);
        ApiMessage apiMessage = ApiMessage.builder().code(ConstantGlobal.ERROR_CODE.UNAUTHORIZED_CODE).content(errorMessage).build();
        return ResponseEntity.badRequest().body(ApiResponse.createResponse(HttpStatus.UNAUTHORIZED
                , Collections.singletonList(apiMessage), null));
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    public ResponseEntity<ApiResponse> handleUsernameNotFoundException(UsernameNotFoundException e, WebRequest request) {
        final String lang = getLanguage(request);
        String errorMessage = MessageUtils.getMessage(e.getMessage(), lang);
        ApiMessage apiMessage = ApiMessage.builder().code(ConstantGlobal.ERROR_CODE.USER_NOT_FOUND_CODE).content(errorMessage).build();
        return ResponseEntity.badRequest().body(ApiResponse.createResponse(HttpStatus.NOT_FOUND
                , Collections.singletonList(apiMessage), null));
    }

    @ExceptionHandler(SignatureException.class)
    public ResponseEntity<ApiResponse> handleSignatureException(SignatureException e, WebRequest request) {
        final String lang = getLanguage(request);
        String errorMessage = MessageUtils.getMessage("jwt.signature.error.message", lang);
        ApiMessage apiMessage = ApiMessage.builder().code(ConstantGlobal.ERROR_CODE.JWT_CODE).content(errorMessage).build();
        return ResponseEntity.badRequest().body(ApiResponse.createResponse(HttpStatus.FORBIDDEN
                , Collections.singletonList(apiMessage), null));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiResponse> handleException(Exception e, WebRequest request) {
        final String lang = getLanguage(request);

        ApiMessage apiMessage = ApiMessage.builder().code("E000").content(e.getMessage()).build();
        return ResponseEntity.badRequest().body(ApiResponse.createResponse(HttpStatus.BAD_REQUEST
                , Collections.singletonList(apiMessage),null));
    }

    public String getLanguage(WebRequest request) {
        String lang = request.getHeader("lang");
        String language = StringUtils.isNotBlank(lang) ? lang : "vi";
        return language;
    }
}
