package com.phiennv.productservcie.controller;

import com.phiennv.productservcie.service.impl.S3FileUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
public class FileController {

    @Autowired
    S3FileUploadService uploadService;

    @PostMapping("/upload/image")
    public String uploadImage(MultipartFile multipartFile) throws IOException {

        return uploadService.uploadFile("images/"+ multipartFile.getOriginalFilename(), multipartFile);
    }

    @GetMapping("/test")
    public String test() {
        return "url file";
    }

}
