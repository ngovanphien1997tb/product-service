package com.phiennv.productservcie.controller;

import com.phiennv.productservcie.dto.ApiResponse;
import com.phiennv.productservcie.dto.ProductRequestForm;
import com.phiennv.productservcie.dto.ProductTestRequestForm;
import com.phiennv.productservcie.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class ProductController {
    @Autowired
    ProductService productService;

    @GetMapping("/{id}")
    public ResponseEntity<ApiResponse> getProduct(@PathVariable Integer id) {
        return ResponseEntity.ok().body(
                ApiResponse.createResponse(HttpStatus.OK
                        , null
                        , productService.getProduct(id))
        );
    }

    @PostMapping("/search")
    public ResponseEntity<ApiResponse> searchProducts(@Valid @RequestBody ProductRequestForm productRequestForm) {
        return ResponseEntity.ok().body(
                ApiResponse.createResponse(HttpStatus.OK
                ,null
                , productService.search(productRequestForm))
        );
    }

    @PostMapping("/create")
    public ResponseEntity<ApiResponse> createProduct(@RequestBody ProductRequestForm requestForm) {
        productService.createProduct(requestForm);
        return ResponseEntity.ok().body(
                ApiResponse.createResponse(HttpStatus.CREATED, null, null));
    }

    @PutMapping("/update")
    public ResponseEntity<ApiResponse> updateProduct(@RequestBody ProductRequestForm requestForm) {
        productService.updateProduct(requestForm);
        return ResponseEntity.ok().body(
                ApiResponse.createResponse(HttpStatus.OK, null, null));
    }

    @DeleteMapping("/delete")
    public ResponseEntity<ApiResponse> deleteProducts(@RequestBody List<Integer> ids) {
        productService.deleteProducts(ids);
        return ResponseEntity.ok().body(
                ApiResponse.createResponse(HttpStatus.OK, null, null));
    }

    @PostMapping("/create-test")
    public ResponseEntity<ApiResponse> createProductTest(@ModelAttribute ProductTestRequestForm requestForm) {
        // productService.createProduct(requestForm);
        return ResponseEntity.ok().body(
                ApiResponse.createResponse(HttpStatus.CREATED, null, null));
    }
}
