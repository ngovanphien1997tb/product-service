package com.phiennv.productservcie.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class ProductRequestForm {

    private String name;

    private String imagesUrl;

    private float price;

    private float rating;

    private String describe;

    private Integer quantity;

    private String listOfColor;
}
