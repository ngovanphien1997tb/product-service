package com.phiennv.productservcie.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.List;

@Data
@AllArgsConstructor
public class ApiResponse<T> {
    private HttpStatus httpStatus;
    private List<ApiMessage> messages;
    private T data;

    public static <T> ApiResponse<T> createResponse( HttpStatus httpStatus,List<ApiMessage> message, T data) {
        return new ApiResponse(httpStatus ,message, data);
    }
}
