package com.phiennv.productservcie.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class ApiMessage {
    private String code;

    private String content;
}
