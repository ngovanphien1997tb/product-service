package com.phiennv.productservcie.dto;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ProductTestRequestForm {
    @NotBlank(message = "validation.name.notnull.message")
    private String name;

    private String imagesUrl;

    @NotNull(message = "validation.price.notnull.message")
    private float price;

    private float rating;

    private String describe;

    @NotNull(message = "validation.quality.notnull.message")
    private Integer quantity;

    private String listOfColor;

    private MultipartFile multipartFile;
}
