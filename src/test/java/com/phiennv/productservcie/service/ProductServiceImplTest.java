package com.phiennv.productservcie.service;

import com.phiennv.productservcie.model.Product;
import com.phiennv.productservcie.repository.ProductRepository;
import com.phiennv.productservcie.service.impl.ProductServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class ProductServiceImplTest {

    @InjectMocks
    ProductServiceImpl productService;

    @Mock
    ProductRepository repository;

    @Test
    public void getProductTest() {
        Product product = new Product();
        product.setId(1);
        Mockito.when(repository.findById(1)).thenReturn(Optional.of(product));

        Product result = productService.getProduct(1);

        assertEquals(result.getId(), product.getId());
    }

}
