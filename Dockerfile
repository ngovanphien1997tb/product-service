FROM openjdk:11-jre-slim

COPY target/product-servcie-0.0.1-SNAPSHOT.jar /product-servcie.jar

EXPOSE 8000

ENTRYPOINT ["java", "-jar", "/product-servcie.jar"]
